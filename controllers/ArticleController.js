import ArticleRepository from "../repository/ArticleRepository.js";

export default class ArticleController {
    static async getArticles(req, res) {
        const articleRepository = new ArticleRepository();

        res.json(await articleRepository.get());
    }
}