
import { Low, JSONFile } from 'lowdb';
import path from 'path';
import _ from 'lodash';

export default class ArticleRepository {
    db;
    file;

    constructor() {
        this.file = path.resolve('./database/articles.json');
        this.db = new Low(new JSONFile(this.file));
    }

    /**
     * 
     * @param {number} id 
     * @returns object
     */
    async find(id) {
        await this.db.read();
 
        const articles = _.get(this.db.data, 'articles', []);

        return articles.find(a => a.id == id);
    }

    /**
     * 
     * @param {number} id 
     * @returns boolean
     */
    async exists(id) {
        return !!ArticleRepository.find(id);
    }

    /**
     * 
     * @returns list of objects
     */
    async get() {
        await this.db.read();
 
        return _.get(this.db.data, 'articles', []);
    }

    /**
     * 
     * @param {object} article 
     */
    async create(article) {
        if(typeof article !== 'object') {
            throw new Error("Article must be an object");
        }

        if(article.id) {
            throw new Error("Article id must be null or undefined");
        }

        await this.db.read();

        const articles = _.get(this.db.data, 'articles', []);

        let counter  = (this.db.data.counter || 0);

        article.id = ++counter;

        articles.push(article);

        this.db.data = {
            ...this.db.data,
            counter,
            articles
        }

        await this.db.write();
    }

    /**
     * 
     * @param {object} article updates article by given id inside the object
     */
    async update(article) {
        if(typeof article !== 'object') {
            throw new Error("Article must be an object");
        }

        if(!article.id) {
            throw new Error("Article id not defined");
        }

        await this.db.read();

        const articles = _.get(this.db.data, 'articles', []);

        let foundArticle = articles.find(a => a.id == article.id);

        if(!foundArticle) {
            throw new Error("Invalid entry");
        }
        
        this.db.data = {
            ...this.db.data,
            articles: articles.map(a => {
                if(a.id == article.id) {
                    return _.merge(foundArticle, article);
                }else {
                    return a;
                }
            })
        }

        await this.db.write();
    }

    /**
     * 
     * @param {number} id 
     */
    async delete(id) {
        if(!id) {
            throw new Error("Article id not defined");
        }

        await this.db.read();

        const articles = _.get(this.db.data, 'articles', []);

        this.db.data = {
            ...this.db.data,
            articles: articles.filter(a => a.id != id)
        }

        await this.db.write();
    }
}