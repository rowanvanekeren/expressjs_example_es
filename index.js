import express from 'express';
import articleRouter from './routes/articles.js';

const app = express();
const port = 3000;

//this middleware is needed to populate the req.body with the incoming json
app.use(express.json());

app.use('/', articleRouter);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
});