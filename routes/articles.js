import express from 'express';
import ArticleController from '../controllers/ArticleController.js';

const router = express.Router();

router.get('/articles', ArticleController.getArticles);

export default router;